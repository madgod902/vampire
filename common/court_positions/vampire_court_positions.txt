﻿vampire_master = {
	skill = intrigue
	max_available_positions = 1
	name = vampire_master_position
	
	custom_employer_modifier_description = vampire_master_employer_desc

	is_shown = {
		has_trait = vampire_underling
	}

	

	is_shown_character = {
		scope:employee = {
			has_trait = vampire
		}
	}

	opinion = {
		value = 120
	}

	salary = {
		gold = {
			value = 0.65
		}
	}

	base_employer_modifier = {
		add_intrigue_skill = 5
	}

	modifier = {
		scheme_power_mult = 0.05
	}

	custom_employee_modifier_description = vampire_master_employee_desc

	on_court_position_recieved = {
		add_character_flag = vampire_master_position
	}
	on_court_position_revoked = {
		remove_character_flag = vampire_master_position
	}
	on_court_position_invalidated = {
		remove_character_flag = vampire_master_position
	}
}